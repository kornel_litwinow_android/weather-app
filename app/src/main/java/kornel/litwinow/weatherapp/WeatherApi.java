package kornel.litwinow.weatherapp;


import kornel.litwinow.weatherapp.Model.Weather;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface WeatherApi {

    //TODO: fetch data by retrofit

    @GET("/data/2.5/weather")
    Call<Weather> getWeather(@Query("q")String city,
                             @Path("appid")String key);

}
