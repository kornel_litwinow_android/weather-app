package kornel.litwinow.weatherapp.Model;

import org.json.JSONException;
import org.json.JSONObject;

public class Weather {

    private String cityName;
    private String temperature;
    private String windSpeed;
    private String humidity;
    private String pressure;
    private int condition;

    public static Weather fromJson(JSONObject jsonObject){
        try{
            Weather weather = new Weather();

            weather.cityName = jsonObject.getString("name");
            weather.condition = jsonObject.getJSONArray("weather").getJSONObject(0).getInt("id");

            double pressureConvert = jsonObject.getJSONObject("main").getDouble("pressure");
            weather.pressure = Double.toString(pressureConvert);

            double windSpeedConvert = jsonObject.getJSONObject("wind").getDouble("speed");
            weather.windSpeed = Double.toString(windSpeedConvert);

            double humidityConvert = jsonObject.getJSONObject("main").getDouble("humidity");
            weather.humidity = Double.toString(humidityConvert);

            double temperatureInKalvin = jsonObject.getJSONObject("main").getDouble("temp");
            double temperatureInCel = temperatureInKalvin - 273;

            int approximateTemperature = (int) Math.round(temperatureInCel);
            weather.temperature = Integer.toString(approximateTemperature);

            return weather;

        }

        catch (JSONException e){
            e.printStackTrace();
            return null;
        }
    }

    public String getCityName() {
        return cityName;
    }

    public String getTemperature() {
        return temperature + "C";
    }

    public String getWindSpeed() {
        return windSpeed + "km/h";
    }

    public String getHumidity() {
        return humidity + "%";
    }

    public String getPressure() {
        return pressure + "hpa";
    }
}
