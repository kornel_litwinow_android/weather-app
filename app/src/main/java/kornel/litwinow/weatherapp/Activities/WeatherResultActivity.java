package kornel.litwinow.weatherapp.Activities;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;
import kornel.litwinow.weatherapp.Model.Weather;
import kornel.litwinow.weatherapp.R;


public class WeatherResultActivity extends AppCompatActivity {

    @BindView(R.id.textView_city)
    TextView textViewCity;
    @BindView(R.id.textView_temperature)
    TextView textViewTemperature;
    @BindView(R.id.textView_windSpeed)
    TextView textViewWindSpeed;
    @BindView(R.id.textView_humidity)
    TextView textViewHumidity;
    @BindView(R.id.textView_pressure)
    TextView textViewPressure;
    @BindView(R.id.button_change_city)
    Button buttonChangeCity;

    public static final String WEATHER_URL = "http://api.openweathermap.org/data/2.5/weather";
    public static final String API_KEY = "0c34719fa85be148acdd0800549c9bf1";
    public static final String CITY_NAME = "cityname";

    String cityName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_result);
        ButterKnife.bind(this);
         cityName = getIntent().getStringExtra(CITY_NAME);
        textViewCity.setText(cityName);

       getCityWeather(cityName);
        backToCityActivity();
    }

    private boolean isConnected(){
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        return networkInfo != null && networkInfo.isConnected();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void backToCityActivity(){
        buttonChangeCity.setOnClickListener(v -> onBackPressed());
    }

    private void getCityWeather(String cityName) {
        RequestParams requestParams = new RequestParams();

        requestParams.put("q", cityName);
        requestParams.put("appid", API_KEY);
        apiCall(requestParams);
    }

    private void apiCall(RequestParams requestParams) {
        if (isConnected()){
            AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
            asyncHttpClient.get(WEATHER_URL,requestParams,new JsonHttpResponseHandler(){
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);
                    Weather weather = Weather.fromJson(response);
                    assert weather != null;
                    updateWeather(weather);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                }
            });
        }
    }

    private void updateWeather(Weather weather) {
        textViewTemperature.setText(weather.getTemperature());
        textViewHumidity.setText(weather.getHumidity());
        textViewPressure.setText(weather.getPressure());
        textViewWindSpeed.setText(weather.getWindSpeed());
    }

}