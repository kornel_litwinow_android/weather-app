package kornel.litwinow.weatherapp.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import kornel.litwinow.weatherapp.R;

public class CityActivity extends AppCompatActivity {

    //Activity fetch city name and pass to WeatherResultActivity

    @BindView(R.id.editText_put_city)
    EditText editTextPutCity;
    @BindView(R.id.button_show_weather)
    Button buttonShowWeather;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city);
        ButterKnife.bind(this);

        getCityName();

    }

    public void getCityName(){

        buttonShowWeather.setOnClickListener(v -> {
            String cityName = editTextPutCity.getText().toString().trim();
            if (!cityName.equals("")){
                Intent intent = new Intent(CityActivity.this, WeatherResultActivity.class);
                intent.putExtra("cityname", cityName);
                startActivity(intent);
            }
        });
    }
}
